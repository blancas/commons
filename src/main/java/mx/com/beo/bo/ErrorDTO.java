package mx.com.beo.bo;
import java.io.Serializable;
public class ErrorDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String result="";
	String response="";
	Integer responseStatus;
	public ErrorDTO(String result, String response, int code) {
		super();
		this.result = result;
		this.response = response;
		this.responseStatus = code;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Integer getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(Integer responseStatus) {
		this.responseStatus = responseStatus;
	}

	
}
