package mx.com.beo.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import mx.com.beo.exceptions.HeaderNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.RequestEntity;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class HeadersParser
{
	private static final Logger LOGGER = LoggerFactory.getLogger(HeadersParser.class);
	private String ticketHeader= "cookie";
	private String ticketField ="PD-S-SESSION-ID";
	private String ticketData ="id_creds";

    /**
    * @param requestHeaders RequestEntity con la informacion de la peticion
    * @return Mapa con la transformacion de headers realizada
    * @throws HeaderNotFoundException
    */
   public Map<String,Object> validaHeaders(RequestEntity requestHeaders) throws HeaderNotFoundException {
	   return validaHeaders(getBasicHeaders(),(Map)requestHeaders.getHeaders().toSingleValueMap(),null);
   }

    /**
     *
     * @param headers Mapa con la definicion de como se realizará la trnasofmacion de los headers
     * @param requestHeaders RequestEntity con la informacion de la peticion
     * @return Mapa con la transformacion de headers realizada
     * @throws HeaderNotFoundException
     */
    public Map<String,Object> validaHeaders(Map<String,Object> headers, RequestEntity requestHeaders) throws HeaderNotFoundException {
        return validaHeaders(headers,(Map)requestHeaders.getHeaders().toSingleValueMap(),null);
    }


    /**
     *
     * @param headers String con el json que define como se realizará la transformacion de nombre y estructura de los headers
     * @param requestHeaders RequestEntity con la informacion de los headers recibidos en la peticion.
     * @return Mapa con el resultado de la tranformacion de acuerdo con el json de defincion
     * @throws HeaderNotFoundException Ocurre cuando no existe alguna cabecera que es requerida.
     * @throws IOException Ocurre cuando el json tiene inconsistencias.
     */
    public Map<String,Object> validaHeaders(String headers, RequestEntity requestHeaders) throws HeaderNotFoundException, IOException {
        ObjectMapper objectMapper= new ObjectMapper();
        Map<String,Object> map= objectMapper.readValue(headers,new TypeReference<Map<String,Object>>(){});
        return validaHeaders(map ,requestHeaders);
    }

	/**
	 *
	 * @param headers Mapa con la definicion de como se realizarala transformacion de los headers
	 * @param requestHeadersMap con la informacion de la peticion
	 * @return Mapa con la transformacion de headers realizada
	 * @throws HeaderNotFoundException
	 */
	public Map<String, Object> validaHeaders(Map requestHeaders) throws HeaderNotFoundException {
		return validaHeaders(getBasicHeaders(), requestHeaders, null);
	}

    /**
     *
     * @param headers Mapa con la defincion de como se realizara la transformacion de nombre y estructura de los headers.
     * @param requestHeaders Mapa con los headers que van a tranformarse
     * @return Map<String,Object> que tiene el resultado de la transoformacion de acuerdo como se indicó en el mapa de definicion
     * @throws HeaderNotFoundException Ocurre cuando una de las cabeceras requeridas no eciste en los headers de la peticion
     */
    public Map<String,Object> validaHeaders(Map<String,Object> headers, Map<String,Object> requestHeaders, Map<String, TiposConversion> configuracionConversion) throws HeaderNotFoundException {
        Map<String, Object> response = new HashMap();
        if(configuracionConversion == null){
        	configuracionConversion = new HashMap();
        	configuracionConversion.put("idPersona", TiposConversion.LONG);
        }

        ArrayList<String> optionalHeaders = headers.containsKey("optional") ? (ArrayList<String>)headers.get("optional"): new ArrayList<>();

        /**
         * Se recorre la lista de headers para realizar la conversión
         */
        for(Entry<String, Object> item: headers.entrySet())
        {
            /**
             * Caso 2: El header no es un string, por lo tanto se requiere envolver headers dentro de un objeto
             */
            if(  !(item.getValue() instanceof String) && !(item.getValue() instanceof List) )
            {
                envolveHeader(item, requestHeaders, response, optionalHeaders, configuracionConversion);
            }
            /**
             * Caso 1: El header existe y su valor es un string en el mapa de transformaciones
             * */
            if(requestHeaders.containsKey(item.getKey()) && item.getValue() instanceof String)
            {
            	if(configuracionConversion.containsKey((String) item.getValue())){
            		convercionTipos(configuracionConversion,response,((String) item.getValue()),requestHeaders.get(item.getKey()));
            		continue;
            	}
            	validateTicketValue(response,(String)item.getValue(), requestHeaders.get(item.getKey()));
            }
            /**
             * Caso Exception: No se encuentra el header requerido
             * */
            else if(!requestHeaders.containsKey(item.getKey()) && item.getValue() instanceof String && !isOptional(item.getKey(),optionalHeaders))
            {
                throw new HeaderNotFoundException("Header not found: " + item.getKey());
            }
        }
        return response;
    }


    private boolean isOptional(String headerName, List<String> optionalHeaders)
    {
        for (String item: optionalHeaders) {
            if(item.equalsIgnoreCase(headerName))
            {
                return true;
            }
        }
        return false;
    }



    /**
     *
     * @param rule Indica como debe armarse el objeto que contiene headers en su interior
     * @param requestHeaders Son los headers que vienen en la peticion
     * @param response Es el mapa donde se deveran envolver los headers que se crean
     * Nota: El metodo es recursivo, por lo que permite crear objetos dentro de objetos de forma indefinida
     */
    private void envolveHeader(Entry<String,Object> rule, Map<String,Object> requestHeaders, Map<String,Object> response, List<String> optionalHeaders, Map<String, TiposConversion> configuracionConversion) throws HeaderNotFoundException {
        Map<String, Object> temp = new HashMap<>();
        String key = rule.getKey();
        Map<String, Object> innerJson = (Map) rule.getValue();

        for (Entry<String, Object> item : innerJson.entrySet()) {
            if (requestHeaders.containsKey(item.getKey()) && item.getValue() instanceof String) {
            	if(configuracionConversion.containsKey((String) item.getValue())){
            		convercionTipos(configuracionConversion,response,((String) item.getValue()),requestHeaders.get(item.getKey()));
            		continue;
            	}
            	validateTicketValue(temp,(String) item.getValue(), requestHeaders.get(item.getKey()));
            }
            /**
             * Caso Exception: No se encuentra el header requerido
             * */
            else if(!requestHeaders.containsKey(item.getKey()) && item.getValue() instanceof String && !isOptional(item.getKey(),optionalHeaders))
            {
                throw new HeaderNotFoundException("Header not found: " + item.getKey());
            }
            else {
                if (!(item.getValue() instanceof String)) {
                    envolveHeader(item, requestHeaders, temp,optionalHeaders, configuracionConversion);
                }
            }
        }
        response.put(key, temp);
    }

    /**
    *
    * @return Map<String,Object> Contiene los headers minimos requeridos para validar
     * @throws HeaderNotFoundException
    */
    private void validateTicketValue(Map<String, Object> map,String key, Object value) throws HeaderNotFoundException{
    	if(key.equals(ticketData)) {
    		Pattern p = Pattern.compile(ticketField + "=.*?[;|\\s]");
    		Matcher m = p.matcher(value.toString().concat(";"));
    		String ticketValue="";
    		if(m.find()) {
    			ticketValue =  m.group().trim().replace(ticketField + "=", "").replace(";","");
    			map.put(key, ticketValue);
    		}else {
    			throw new HeaderNotFoundException("Header not found in "+ ticketHeader  + ":" + ticketField);
    		}
    	}
    	else {
    		map.put(key, value);
    	}
    }


    /**
    *
    * @return Map<String,Object> Contiene los headers minimos requeridos para validar
    */
    private Map<String,Object> getBasicHeaders(){
    	Map<String, Object> headersBasicos = new HashMap<>();
		Map<String, Object> ticket = new HashMap<>();
		ticket.put("iv-user", "id_user");
		ticket.put(ticketHeader,ticketData);
		headersBasicos.put("ticket", ticket);
		headersBasicos.put("tipocanal", "canal");
        headersBasicos.put("iv-user", "idPersona");
        headersBasicos.put("numero-cliente", "cliente");
		return headersBasicos;
    }

    /**
    *
    * @param configuracionConversion mapa donde KEY es el nombre del campo del Body y VALUE es un enum TiposConversion que tiene los objetos destino (String, Integer,FLOAT,...)
    * @param destino mapa donde se insertaran los valores procesados
    * @param key KEY para el mapa destino
    * @param value VALUE para el mapa destino
    */
    private void convercionTipos(Map<String,TiposConversion> configuracionConversion,Map<String, Object> destino, String key, Object value){
    	if(configuracionConversion.containsKey(key)){
        	switch (configuracionConversion.get(key)) {
			case STRING:
				break;
			case INTEGER:
				destino.put(key, Integer.parseInt((String)value));
				break;
			case LONG:
				destino.put(key, Long.parseLong((String)value));
				break;
			case DOUBLE:
				destino.put(key, Double.parseDouble((String)value));
				break;
			case FLOAT:
				destino.put(key, Float.parseFloat((String)value));
				break;
			case BOOLEAN:
				destino.put(key, Boolean.parseBoolean((String)value));
				break;
			default:
				destino.put(key, value);
				break;
			}
        } else{
       		destino.put(key, value);
       	}
    }
}
