package mx.com.beo.util;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import mx.com.beo.bo.ErrorDTO;
import mx.com.beo.exceptions.HeaderNotFoundException;
public class UtilidadesRest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(UtilidadesRest.class);
    private int timeoutRead;
    private int timeoutRequestConn;
    private int timeoutConnection;
    private Integer[] erroresOTP = null;
    private Integer[] erroresSesion = null;
    private Map<String, Object> mapaErrores = null;
    private String errorAsyncrono;
    public UtilidadesRest()
    {
    	this.mapaErrores =new HashMap<>();
    	this.erroresOTP = new Integer[]{2431,2423,2441,2442,2443,2444,2422,2426};
    	this.erroresSesion = new Integer[]{2003,2004,2002};
    	this.mapaErrores.put("erroresOTP", erroresOTP);
    	this.mapaErrores.put("erroresSesion", erroresSesion);
        this.timeoutRead=90000;
        this.timeoutConnection=90000;
        this.timeoutRequestConn=90000;
    }

    private AsyncRestTemplate generateAsyncRestTemplate(int readTimeout,int connTimeout, int requestConnTimeout)
    {
        HttpComponentsAsyncClientHttpRequestFactory httpComponentsAsyncClientHttpRequestFactory= new HttpComponentsAsyncClientHttpRequestFactory();
        httpComponentsAsyncClientHttpRequestFactory.setReadTimeout(readTimeout);
        httpComponentsAsyncClientHttpRequestFactory.setConnectionRequestTimeout(requestConnTimeout);
        httpComponentsAsyncClientHttpRequestFactory.setConnectTimeout(connTimeout);
        return new AsyncRestTemplate(httpComponentsAsyncClientHttpRequestFactory);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Map restMultiples(Map<String, Object> restRequests, String servicio, String urlBitacora, HttpHeaders httpHeadersBitacora)
    {
    	boolean restMultiplesError = false;
        BitacoraConsumoServicio bitacoraConsumoServicio = new BitacoraConsumoServicio();
        AsyncRestTemplate asyncRestTemplate;
        Map<String, Object> respuesta = new HashMap();
        ListenableFuture<ResponseEntity<Object>> future=null;
        List<Multiples> multipleList = new ArrayList<>();
        List<String> links = new ArrayList<>(); 
        Map<String, Object> erroresPeticion = new HashMap();
        ArrayList<mx.com.beo.exceptions.Error> errores = new ArrayList<mx.com.beo.exceptions.Error>();
        String  erroresSTR ="";
        for(Map.Entry<String,Object> request : restRequests.entrySet())
        {

            Map<String,Object> informacionRest=(Map) request.getValue();
            String url= (String) informacionRest.get("endpoint");
            String method= (String)informacionRest.get("method");
            Map<String, Object> body = (Map) informacionRest.get("body");
            Map<String,Object> headersMap= (Map) informacionRest.get("headers");
            HttpHeaders httpHeaders= new HttpHeaders(); 
            bitacoraConsumoServicio.setService(bitacoraConsumoServicio.getMethodFromURL(url));
            bitacoraConsumoServicio.setIpUser(bitacoraConsumoServicio.getHostname());
            bitacoraConsumoServicio.setRequestValue(mapToJson(body));
            bitacoraConsumoServicio.setHttpHeaders(httpHeadersBitacora);
            bitacoraConsumoServicio.setClientNumber(body.containsKey("cliente")?body.get("cliente").toString():"");

            for(Map.Entry<String,Object> entry : headersMap.entrySet())
            {
                httpHeaders.add(entry.getKey(), (String) entry.getValue());
            }

            HttpEntity<Object> httpEntity= new HttpEntity<>(body, httpHeaders);
            if(informacionRest.containsKey("readTimeout") && informacionRest.containsKey("connectTimeout") && informacionRest.containsKey("connectionRequestTimeout") )
            {
                int readTimeout= (Integer)informacionRest.get("readTimeout");
                int connTimeout= (Integer) informacionRest.get("connectTimeout");
                int connRequestTimeout= (Integer) informacionRest.get("connectionRequestTimeout");
                bitacoraConsumoServicio.setRequestDate(new Date());

                asyncRestTemplate = generateAsyncRestTemplate(readTimeout,connTimeout,connRequestTimeout);
                bitacoraConsumoServicio.setResponseDate(new Date());
            }
            else
            {
            	bitacoraConsumoServicio.setRequestDate(new Date());
                asyncRestTemplate = generateAsyncRestTemplate(this.timeoutRead,this.timeoutConnection,this.timeoutRequestConn);
                bitacoraConsumoServicio.setResponseDate(new Date());
            }

            Multiples multiple = new Multiples();
            future= asyncRestTemplate.exchange(url, HttpMethod.valueOf(method),httpEntity,Object.class);
            multiple.setFuture(future);
            multiple.setUrl(url);
            multiple.setRequest(mapToJson(body));
			multiple.setKeyRest(request.getKey());
			multipleList.add(multiple); 
			
 
        } 
		while(!multipleList.isEmpty()) {
			for (int i = 0; i < multipleList.size(); i++) {
				String response="", nombreServicio="", urlServicio="", request="";
				try {
                 /**
                	 * En caso de que tarde en responder el servicio nos manda un
                	 * WARN donde nos indica que tal servicio esta tardando en
                	 * responder.
                	 * Ejemplo:
                	 * Async POST request for "http://localhost:9094/envioNotificaciones"
                	 *
                	 * Si el servicio no logra responder en el tiempo que le asigna manda la excepción
                	 *
                	 * ExecutionException org.springframework.web.client.HttpServerErrorException: 500 Internal Server Error
                	 */
					urlServicio = multipleList.get(i).getUrl();
					nombreServicio = multipleList.get(i).getKeyRest();
					request = multipleList.get(i).getRequest();

					Map<String, Object> temp= new HashMap<>();
					temp.put("body", multipleList.get(i).getFuture().get().getBody());
					temp.put("headers", multipleList.get(i).getFuture().get().getHeaders());

					Map<String, Object> res = new HashMap<>();
					res.put(multipleList.get(i).getKeyRest(), temp);
					respuesta.putAll(res);

					response =  mapToJson(respuesta);
					
					LOGGER.info("Manda Errores ::: "+errores.size());
			        LOGGER.info("Manda Errores ::: "+errores);
					
					LOGGER.info("response >>"+response);
					imprimeBitacora(urlServicio,multipleList.get(i).getRequest(), multipleList.get(i).getFuture().get().getBody().toString());
				}
				catch (InterruptedException e)
				{
					LOGGER.error("InterruptedException {}", e.getMessage());
					Thread.currentThread().interrupt();
				}
				catch (ExecutionException executionException) 
				{
					if(!(urlServicio.indexOf("envioNotificaciones")>0))
					{
						mx.com.beo.exceptions.Error error=new mx.com.beo.exceptions.Error("Error."+executionException.getMessage(),500,urlServicio);
						errores.add(error);
					}
					LOGGER.error("Error en restMultiples ["+urlServicio+"]", executionException);
					//validaException(executionException, nombreServicio, urlServicio, request , response ,bitacoraConsumoServicio);
				}
				catch (Exception e) 
				{
					LOGGER.error("Error en restMultiples ", e);
				}finally {
					multipleList.remove(i);
				}
			}
		}
	
        if(!restMultiplesError){
        	bitacoraConsumoServicio.setResponseValue(mapToJson(respuesta));
        	bitacoraConsumoServicio.registroBitacora(urlBitacora,servicio);
        }
		if(errores.size()>0)
        {
			for(int indexError=0;indexError<errores.size();indexError++)
			{
				mx.com.beo.exceptions.Error error=errores.get(indexError);
				erroresSTR=erroresSTR+" ["+error.toString()+"]";
			}
			erroresPeticion.put("Error",erroresSTR);
        	LOGGER.info("Manda Errores "+erroresSTR);
        	return erroresPeticion;
        }
        return respuesta;
    }

    /**
    *
    * @param url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param cabeceraPeticion mapa que indica el header del request
    * @param cuerpoPeticion mapa que indica el body del request
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticion(String url, HttpMethod verboHttp, Set<MediaType> responseContentTypeValidos, Map<String, Object> cabeceraPeticion, Map<String, Object> cuerpoPeticion){
    	return enviarPeticion(url,verboHttp, responseContentTypeValidos, cabeceraPeticion, cuerpoPeticion,false, this.timeoutRead,this.timeoutConnection,"", null);
    }

    /**
    *
    * @param url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param cabeceraPeticion mapa que indica el header del request
    * @param cuerpoPeticion mapa que indica el body del request
    * @param urlBitacora URL donde esta el microservicio de bitacora (historial)
    * @param httpHeadersBitacora headers para la invocacion al microservicio de bitacora (historial)
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticion(String url, HttpMethod verboHttp, Set<MediaType> responseContentTypeValidos, Map<String, Object> cabeceraPeticion, Map<String, Object> cuerpoPeticion, String urlBitacora, HttpHeaders httpHeadersBitacora){
    	return enviarPeticion(url,verboHttp, responseContentTypeValidos, cabeceraPeticion, cuerpoPeticion,false, this.timeoutRead,this.timeoutConnection,urlBitacora,httpHeadersBitacora);
    }

    /**
    *
    * @param url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param responseContentTypeValidos Content-Type validos
    * @param cuerpoPeticion mapa que indica el body del request
    * @param urlBitacora URL donde esta el microservicio de bitacora (historial)
    * @param httpHeadersBitacora headers para la invocacion al microservicio de bitacora (historial)
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticionCSV(
    		String url,
    		HttpMethod verboHttp,
    		Set<MediaType> responseContentTypeValidos,
    		Map<String, Object> cuerpoPeticion,
    		String urlBitacora,
    		HttpHeaders httpHeadersBitacora){

    	LOGGER.debug("enviarPeticionSCV");
    	return getPeticionCSV(url,verboHttp, responseContentTypeValidos, cuerpoPeticion, this.timeoutRead,this.timeoutConnection,urlBitacora,httpHeadersBitacora);
    }

    /**
    *
    * @param url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param responseContentTypeValidos Content-Type validos
    * @param cuerpoPeticion mapa que indica el body del request
    * @param readTimeout  ReadTimeout para el request enviado
    * @param connectTimeout ConnectionTimeout para el request enviado
    * @param urlBitacora URL donde esta el microservicio de bitacora (historial)
    * @param httpHeadersBitacora headers para la invocacion al microservicio de bitacora (historial)
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> getPeticionCSV(
    		String url,
    		HttpMethod verboHttp,
    		Set<MediaType> responseContentTypeValidos,
    		Map<String, Object> cuerpoPeticion,
    		int readTimeout,
    		int connectTimeout,
    		String urlBitacora,
    		HttpHeaders httpHeadersBitacora){

    	LOGGER.info("getPeticionCSV");

    	RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders= new HttpHeaders();
		HttpEntity httpEntity = null;
		BitacoraConsumoServicio bitacoraConsumoServicio = new BitacoraConsumoServicio();
    	boolean contentypeValidoEncontrado = false;
    	ResponseEntity response=null;
		ResponseEntity<String> responseApplicationOctetStream = null;
		ResponseEntity<Object> responseServicio=null;
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		rf.setReadTimeout(readTimeout);
		rf.setConnectTimeout(connectTimeout);
		restTemplate.setRequestFactory(rf);

		httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
		httpHeaders.setContentType(new MediaType("text", "csv"));
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

        httpEntity = new HttpEntity<Object>( cuerpoPeticion, httpHeaders );


		if(LOGGER.isDebugEnabled()){
        	String request = httpEntity.getBody().toString();
        	LOGGER.debug("ESB request: {}", request);
        }
        String clientNumber = "";
        try{
        	clientNumber = ((Map<String,Object>)((Map<String,Object>)httpEntity.getBody()).get("ticket")).get("id_user").toString();
        }catch (Exception e) {
        	clientNumber = "";
		}

        LOGGER.debug("clientNumber: {}", clientNumber);

        bitacoraConsumoServicio.setService(bitacoraConsumoServicio.getMethodFromURL(url));
        bitacoraConsumoServicio.setIpUser(bitacoraConsumoServicio.getHostname());
        bitacoraConsumoServicio.setRequestValue(mapToJson((Map<String,Object>)httpEntity.getBody()));
        bitacoraConsumoServicio.setClientNumber(clientNumber);
        bitacoraConsumoServicio.setRequestDate(new Date());
        bitacoraConsumoServicio.setHttpHeaders(httpHeadersBitacora);
        try{

        		try{

        			LOGGER.error("url: {}", url);
        			LOGGER.error("verboHttp: {}", verboHttp);
        			LOGGER.error("httpEntity: {}", httpEntity);
        			responseApplicationOctetStream = restTemplate.exchange(url, verboHttp, httpEntity, String.class);
        			bitacoraConsumoServicio.setResponseDate(new Date());
        			response = responseApplicationOctetStream;

        		} catch (Exception e) {
        			return getErrorResponse(500,Constantes.ERROR_SOLICITUD,"Problemas al consumir text/csv:, "+ e.toString());
				}


        	if(response!=null && !response.getStatusCode().equals(HttpStatus.OK)){
        		LOGGER.error("Error status");
        	}


        	if(response!=null)
        	for(MediaType mediaTypeItem : responseContentTypeValidos){
        		if(mediaTypeItem.equals(response.getHeaders().getContentType())){
        			contentypeValidoEncontrado=true;
        			break;
        		}
        	}
        	LOGGER.debug("URL Bitacora: {}",urlBitacora);
        	bitacoraConsumoServicio.loggger(true,urlBitacora);

        	if(!contentypeValidoEncontrado){
        		bitacoraConsumoServicio.setResponseDate(new Date());
        		bitacoraConsumoServicio.loggger(false);
        		String actualHeader = (response!=null && response.getHeaders()!=null) ? response.getHeaders().getContentType().toString():"null";
        		return getErrorResponse(500,Constantes.ERROR_SOLICITUD,"Content-Type inesperado: {}" + actualHeader);
        	}

        }catch (ResourceAccessException resourceException) {
    		return validaException(resourceException, url, bitacoraConsumoServicio);

        } catch(Exception e){
        	LOGGER.error("Error al invocar el servicio {} con la excepcion: {}", bitacoraConsumoServicio.getMethodFromURL(url), e.getMessage());
        	bitacoraConsumoServicio.setResponseDate(new Date());
        	bitacoraConsumoServicio.loggger(false);
        	return getErrorResponse(500,Constantes.ERROR_SOLICITUD,e.toString());
        }


        HashMap<String,Object> entity=new HashMap<String,Object>();
        entity.put("contents",responseApplicationOctetStream.getBody());
        entity.put("responseStatus",Integer.valueOf(200));
        entity.put("responseError","");

        HttpHeaders httpHeadersResp= new HttpHeaders();
        httpHeadersResp.setAccept(Arrays.asList(MediaType.ALL));
        httpHeadersResp.setContentType(MediaType.APPLICATION_JSON);
        httpHeadersResp.setContentType(MediaType.APPLICATION_JSON_UTF8);


        return new ResponseEntity<>(entity, httpHeadersResp, HttpStatus.OK );
    }

    /**
    *
    * @param url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param cabeceraPeticion mapa que indica el header del request
    * @param cuerpoPeticion mapa que indica el body del request
    * @param isApplicationOctetStream cuando es true indica que espera consultar un APPLICATION_OCTET_STREAM, false
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticion(String url, HttpMethod verboHttp, Set<MediaType> responseContentTypeValidos, Map<String, Object> cabeceraPeticion, Map<String, Object> cuerpoPeticion, boolean isApplicationOctetStream){
    	return enviarPeticion(url,verboHttp, responseContentTypeValidos, cabeceraPeticion, cuerpoPeticion,isApplicationOctetStream, this.timeoutRead,this.timeoutConnection,"",null);
    }

    /**
    *
    * @param url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param cabeceraPeticion mapa que indica el header del request
    * @param cuerpoPeticion mapa que indica el body del request
    * @param isApplicationOctetStream cuando es true indica que espera consultar un APPLICATION_OCTET_STREAM, false
    * @param urlBitacora URL donde esta el microservicio de bitacora (historial)
    * @param httpHeadersBitacora headers para la invocacion al microservicio de bitacora (historial)
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticion(String url, HttpMethod verboHttp, Set<MediaType> responseContentTypeValidos, Map<String, Object> cabeceraPeticion, Map<String, Object> cuerpoPeticion, boolean isApplicationOctetStream, String urlBitacora, HttpHeaders httpHeadersBitacora){
    	return enviarPeticion(url,verboHttp, responseContentTypeValidos, cabeceraPeticion, cuerpoPeticion,isApplicationOctetStream, this.timeoutRead,this.timeoutConnection,urlBitacora,httpHeadersBitacora);
    }

    /**
    *
    * @param url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param cabeceraPeticion mapa que indica el header del request
    * @param cuerpoPeticion mapa que indica el body del request
    * @param isApplicationOctetStream cuando es true indica que espera consultar un APPLICATION_OCTET_STREAM, false
    * @param readTimeout  ReadTimeout para el request enviado
    * @param connectTimeout ConnectionTimeout para el request enviado
    * @param urlBitacora URL donde esta el microservicio de bitacora (historial)
    * @param httpHeadersBitacora headers para la invocacion al microservicio de bitacora (historial)
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticion(String url, HttpMethod verboHttp, Set<MediaType> responseContentTypeValidos, Map<String, Object> cabeceraPeticion, Map<String, Object> cuerpoPeticion, boolean isApplicationOctetStream,int readTimeout, int connectTimeout, String urlBitacora, HttpHeaders httpHeadersBitacora){
    	RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders= new HttpHeaders();
		HttpEntity httpEntity = null;
		BitacoraConsumoServicio bitacoraConsumoServicio = new BitacoraConsumoServicio();
    	boolean contentypeValidoEncontrado = false;
    	ResponseEntity response=null;
		ResponseEntity<byte[]> responseApplicationOctetStream = null;
		ResponseEntity<Object> responseServicio=null;
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		rf.setReadTimeout(readTimeout);
		rf.setConnectTimeout(connectTimeout);
		restTemplate.setRequestFactory(rf);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM,MediaType.APPLICATION_JSON,MediaType.APPLICATION_JSON_UTF8));


		if(cabeceraPeticion!=null)
		for(Map.Entry<String,Object> entry : cabeceraPeticion.entrySet())
        {
            httpHeaders.add(entry.getKey(), (String) entry.getValue());
        }


        httpEntity = new HttpEntity<Object>( cuerpoPeticion, httpHeaders );


		if(LOGGER.isDebugEnabled()){
        	String request = httpEntity.getBody().toString();
        	LOGGER.debug("ESB request:{}", request);
        }
        String clientNumber = "";
        try{
        	clientNumber = ((Map<String,Object>)((Map<String,Object>)httpEntity.getBody()).get("ticket")).get("id_user").toString();
        }catch (Exception e) {
        	clientNumber = "";
		}

        bitacoraConsumoServicio.setService(bitacoraConsumoServicio.getMethodFromURL(url));
        bitacoraConsumoServicio.setIpUser(bitacoraConsumoServicio.getHostname());
        bitacoraConsumoServicio.setRequestValue(mapToJson((Map<String,Object>)httpEntity.getBody()));
        bitacoraConsumoServicio.setClientNumber(clientNumber);
        bitacoraConsumoServicio.setRequestDate(new Date());
        bitacoraConsumoServicio.setHttpHeaders(httpHeadersBitacora);
        try{
        	if(isApplicationOctetStream){
        		try{

        			responseApplicationOctetStream = restTemplate.exchange(url, verboHttp, httpEntity, byte[].class, "1");
        			bitacoraConsumoServicio.setResponseDate(new Date());
        			response = responseApplicationOctetStream;
        		} catch (Exception e) {
        			return getErrorResponse(500,Constantes.ERROR_SOLICITUD,"Problemas al consumir APPLICATION_OCTET_STREAM, "+e.toString());
				}
        	}else{
        		responseServicio = restTemplate.exchange(url, verboHttp, httpEntity, Object.class);
        		bitacoraConsumoServicio.setResponseDate(new Date());
        		bitacoraConsumoServicio.setResponseValue(mapToJson((Map<String,Object>)responseServicio.getBody()));
        		response = responseServicio;
        	}

        	if(response!=null && !response.getStatusCode().equals(HttpStatus.OK)){
        		LOGGER.error("Error status");
        	}


        	if(response!=null)
        	for(MediaType mediaTypeItem : responseContentTypeValidos){
        		if(mediaTypeItem.equals(response.getHeaders().getContentType())){
        			contentypeValidoEncontrado=true;
        			break;
        		}
        	}
        	LOGGER.debug("URL Bitacora: {}",urlBitacora);
        	bitacoraConsumoServicio.loggger(true,urlBitacora);

        	if(!contentypeValidoEncontrado){
        		bitacoraConsumoServicio.setResponseDate(new Date());
        		bitacoraConsumoServicio.loggger(false);
        		String actualHeader = (response!=null && response.getHeaders()!=null) ? response.getHeaders().getContentType().toString():"null";
        		return getErrorResponse(500,Constantes.ERROR_SOLICITUD,"Content-Type inesperado: {}" + actualHeader);
        	}

        }catch (ResourceAccessException resourceException) {

    			return validaException(resourceException, url, bitacoraConsumoServicio);

        } catch(Exception e){
        	LOGGER.error("Error al invocar el servicio {} con la excepcion: {}", bitacoraConsumoServicio.getMethodFromURL(url), e.getMessage());
        	bitacoraConsumoServicio.setResponseDate(new Date());
        	bitacoraConsumoServicio.loggger(false);
        	return getErrorResponse(500,Constantes.ERROR_SOLICITUD,e.toString());
        }
        ResponseEntity.status(HttpStatus.BAD_REQUEST);

        if(isApplicationOctetStream)
        		return new ResponseEntity(responseApplicationOctetStream.getBody(), responseApplicationOctetStream.getHeaders(), HttpStatus.OK );
        else {
        		HttpStatus estatus = manejoErrores(responseServicio);
        		return new ResponseEntity<>(responseServicio.getBody(),responseServicio.getHeaders(),estatus);
        }
    }

    /**
    *
    * @param 1url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param MultipartHttpServletRequest mapa que indica el header del request
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
     * @throws IOException
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticion(String url, HttpMethod verboHttp, Set<MediaType> mediaTypeValidos, Map<String, Object>  cabeceraPeticion , MultiValueMap<String, Object> multi, boolean isApplicationOctetStream, String urlBitacora, HttpHeaders headers){

    	return enviarPeticion(url,verboHttp, mediaTypeValidos,cabeceraPeticion, multi, isApplicationOctetStream, this.timeoutRead, this.timeoutConnection, urlBitacora,headers);
    }
    /**
    *
    * @param 1url String que indica la URL del endpoint al que se le hace la solicitud
    * @param verboHttp Indica el tipo de metodo HTTP que se utilizara en la peticion, ejemplo HttpMethod.POST
    * @param MultipartHttpServletRequest mapa que indica el header del request
    * @return ResponseEntity respuesta del servicio solicitado, en caso de error responde {Constantes.RESPONSE_STATUS:500,Constantes.RESPONSE_ERROR:"detalle"}
     * @throws IOException
    * @throws HeaderNotFoundException
    */
    public ResponseEntity<Object> enviarPeticion(String url, HttpMethod verboHttp, Set<MediaType> responseContentTypeValidos, Map<String, Object>  cabeceraPeticion, MultiValueMap<String, Object> cuerpoPeticion, boolean isApplicationOctetStream,int readTimeout, int connectTimeout, String urlBitacora, HttpHeaders httpHeadersBitacora){

    	RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders= new HttpHeaders();
		@SuppressWarnings("rawtypes")
		HttpEntity httpEntity = null;
		BitacoraConsumoServicio bitacoraConsumoServicio = new BitacoraConsumoServicio();
    	boolean contentypeValidoEncontrado = false;
    	ResponseEntity response=null;
		ResponseEntity<byte[]> responseApplicationOctetStream = null;
		ResponseEntity<Object> responseServicio=null;
		restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory());
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		rf.setReadTimeout(readTimeout);
		rf.setConnectTimeout(connectTimeout);
		httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

		if(cabeceraPeticion!=null)
		for(Map.Entry<String,Object> entry : cabeceraPeticion.entrySet())
        {
            httpHeaders.add(entry.getKey(), (String) entry.getValue());
        }

        httpEntity = new HttpEntity<Object>( cuerpoPeticion, httpHeaders );


		if(LOGGER.isDebugEnabled()){
        	String request = httpEntity.getBody().toString();
        	LOGGER.debug("ESB request:{}", request);
        }
        String clientNumber = "";
        try{
        	clientNumber = ((Map<String,Object>)((Map<String, Object>)httpEntity.getBody()).get("ticket")).get("id_user").toString();
        }catch (Exception e) {
        	clientNumber = "";
		}

        bitacoraConsumoServicio.setService(bitacoraConsumoServicio.getMethodFromURL(url));
        bitacoraConsumoServicio.setIpUser(bitacoraConsumoServicio.getHostname());
        bitacoraConsumoServicio.setRequestValue(mapToJson((Map<String,Object>)httpEntity.getBody()));
        bitacoraConsumoServicio.setClientNumber(clientNumber);
        bitacoraConsumoServicio.setRequestDate(new Date());
        bitacoraConsumoServicio.setHttpHeaders(httpHeadersBitacora);
        try{
        	if(isApplicationOctetStream){
        		try{

        			responseApplicationOctetStream = restTemplate.exchange(url, verboHttp, httpEntity, byte[].class, "1");
        			bitacoraConsumoServicio.setResponseDate(new Date());
        			response = responseApplicationOctetStream;
        		} catch (Exception e) {
        			return getErrorResponse(500,Constantes.ERROR_SOLICITUD,"Problemas al consumir APPLICATION_OCTET_STREAM, "+e.toString());
				}
        	}else{
        		responseServicio = restTemplate.exchange(url, verboHttp, httpEntity, Object.class);

        		bitacoraConsumoServicio.setResponseDate(new Date());
        		bitacoraConsumoServicio.setResponseValue(mapToJson((Map<String,Object>)responseServicio.getBody()));
        		response = responseServicio;
        	}
        	if(response!=null && !response.getStatusCode().equals(HttpStatus.OK)){
        		LOGGER.error("Error status");
        	}

        	if(response!=null)
        	for(MediaType mediaTypeItem : responseContentTypeValidos){
        		if(mediaTypeItem.equals(response.getHeaders().getContentType())){
        			contentypeValidoEncontrado=true;
        			break;
        		}
        	}
        	LOGGER.debug("URL Bitacora: {}",urlBitacora);
        	bitacoraConsumoServicio.loggger(true,urlBitacora,"MULTIPART_FORM");
        	if(!contentypeValidoEncontrado){
        		bitacoraConsumoServicio.setResponseDate(new Date());
        		bitacoraConsumoServicio.loggger(false);
        		String actualHeader = (response!=null && response.getHeaders()!=null) ? response.getHeaders().getContentType().toString():"null";
        		return getErrorResponse(500,Constantes.ERROR_SOLICITUD,"Content-Type inesperado: {}" + actualHeader);
        	}

        }catch (ResourceAccessException resourceException) {

    			return validaException(resourceException, url, bitacoraConsumoServicio);

        } catch(Exception e){
        	LOGGER.error("Error al invocar el servicio {} con la excepcion: {}", bitacoraConsumoServicio.getMethodFromURL(url), e.getMessage());
        	bitacoraConsumoServicio.setResponseDate(new Date());
        	bitacoraConsumoServicio.loggger(false);
        	return getErrorResponse(500,Constantes.ERROR_SOLICITUD,e.toString());
        }
        ResponseEntity.status(HttpStatus.BAD_REQUEST);

        if(isApplicationOctetStream)
        		return new ResponseEntity(responseApplicationOctetStream.getBody(), responseApplicationOctetStream.getHeaders(), HttpStatus.OK );
        else {
        		HttpStatus estatus = manejoErrores(responseServicio);
            LOGGER.debug("ResponseSTATUS: {}", responseServicio.getStatusCode().toString());
        		return new ResponseEntity<>(responseServicio.getBody(),responseServicio.getHeaders(),estatus);
        }
    }



    /**
    *
    * @param responseStatus HTTP Status en la respuesta JSON
    * @param responseError Mensaje de error en la respuesta JSON
    * @param errorLog Mensaje de error para el log
    * @return ResponseEntity respuesta de error controlado
    */
    public ResponseEntity<Object> getErrorResponse(int responseStatus, String responseError, String errorLog){
		return getErrorResponse(responseStatus,responseError,errorLog,HttpStatus.OK);
    }

    /**
    *
    * @param responseStatus HTTP Status en la respuesta JSON
    * @param responseError Mensaje de error en la respuesta JSON
    * @param errorLog Mensaje de error para el log
    * @param httpStatus enumeracion tipo HttpStatus para indicar el codigo del HTTP status del response
    * @return ResponseEntity respuesta de error controlado
    */
    public ResponseEntity<Object> getErrorResponse(int responseStatus, String responseError, String errorLog, HttpStatus httpStatus){
    	Map<String, Object> responseErrorMap = new HashMap<>();
    	responseErrorMap.put(Constantes.RESPONSE_STATUS, responseStatus);
    	responseErrorMap.put(Constantes.RESPONSE_ERROR, responseError);
		LOGGER.error(errorLog);
		return new ResponseEntity<>(responseErrorMap,httpStatus);
    }

    /**
    *
    * @param responseStatus HTTP Status en la respuesta JSON
    * @param responseError Mensaje de error en la respuesta JSON
    * @param httpStatus enumeracion tipo HttpStatus para indicar el codigo del HTTP status del response
    * @return ResponseEntity objeto de respuesta
    */
    public ResponseEntity<Object> createResponseEntity(int responseStatus, String responseError, HttpStatus httpStatus){
    	Map<String, Object> responseMap = new HashMap<>();
    	responseMap.put(Constantes.RESPONSE_STATUS, responseStatus);
    	responseMap.put(Constantes.RESPONSE_ERROR, responseError);
    	return new ResponseEntity<>(responseMap,httpStatus);
    }

    /**
    *
    * @param responseStatus HTTP Status en la respuesta JSON
    * @param llaveResponse Mensaje con la que se mostrara la peticion
    * @param responseError Mensaje de error en la respuesta JSON
    * @param httpStatus enumeracion tipo HttpStatus para indicar el codigo del HTTP status del response
    * @return ResponseEntity objeto de respuesta
    */
    public ResponseEntity<Object> createResponseEntity(int responseStatus, String llaveResponse, Object response, String responseError, HttpStatus httpStatus){
    	Map<String, Object> responseMap = new HashMap<>();
    	responseMap.put(llaveResponse, response);
    	responseMap.put(Constantes.RESPONSE_STATUS, responseStatus);
    	responseMap.put(Constantes.RESPONSE_ERROR, responseError);
    	return new ResponseEntity<>(responseMap,httpStatus);
    }

    /**
    *
    * @param responseStatus HTTP Status en la respuesta JSON
    * @param responseError Mensaje de error en la respuesta JSON
    * @param errorLog Mensaje de error para el log
    * @param httpStatus enumeracion tipo HttpStatus para indicar el codigo del HTTP status del response
    * @return ResponseEntity respuesta de error controlado
    */
    private HttpStatus manejoErrores(ResponseEntity<Object> respuesta){
    		return validarResponseStatus(respuesta,mapaErrores,HttpStatus.FORBIDDEN);
    }

    private HttpStatus validarResponseStatus(ResponseEntity<Object> respuesta,Map<String, Object> mapaErrores,HttpStatus estatus){
    		Map<String, Object> mapaRespuesta = (Map<String, Object>) respuesta.getBody();
    		for (Entry entry: mapaErrores.entrySet()) {
    			Integer[] arregloErrores = (Integer[]) entry.getValue();
    			if(mapaRespuesta.containsKey(Constantes.RESPONSE_STATUS)) {
    				for (Integer codigoError : arregloErrores) {
    					if(mapaRespuesta.get(Constantes.RESPONSE_STATUS).equals(codigoError)) {
    						return estatus;
    					}
    				}
    			}
		}
		return HttpStatus.OK;
    }

    /**
    * @param json texto en formato JSON
    * @return HashMap<String, Object> HashMap que representa la conversion del JSON a un mapa.
    */
	public static Map<String,Object> jsonToMap(String json){
		Map<String, Object> map = new HashMap<>();
	    ObjectMapper mapper = new ObjectMapper();
	    try {
	    	//convert JSON string to Map
	        map = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {});
	        return map;
	    } catch (Exception e) {
	       LOGGER.error("jsonToMap: {}", e.getMessage());
	    }
	    return null;
	}

	/**
    * @param map HashMap que representa a un JSON
    * @return texto en formato JSON
    */
	public static String mapToJson(Map<String,Object> map){
		String json = "";
		try{
			Gson gson = new Gson();
			json = gson.toJson(map);
		}catch (Exception e) {
			json = "";
		}
		return json.replace(":[", ":").replace("],", ",").replace("]}", "}");
	}

	/**
	    * @param map MultiValueMap que representa a un JSON
	    * @return texto en formato JSON
	    */
	public String mapToJson(MultiValueMap<String, Object> body) {
		  String json = "";
			try{
				Gson gson = new Gson();
				json = gson.toJson(body);
			}catch (Exception e) {
				json = "";
			}
			return json;
		}


	/**
	 * Valida que tipo de Exception que se presenta mediante ResourceAccessException
	 *
	 * @param ResourceAccessException Exception generada por no respuesta del Servicio
	 * @param bitacoraConsumoServicio Objeto para el almacenamiento de Logs
	 *
	 * @return validaException <strong>ResponseEntity</strong> Contiene el mensaje de Error generado, as&iacute; como el estado HTTP y su descripci&oacute;n
	 */
	public ResponseEntity<Object> validaException(ResourceAccessException resourceException,
			String urlServicio, BitacoraConsumoServicio bitacoraConsumoServicio) {

		bitacoraConsumoServicio.setResponseDate(new Date());
		bitacoraConsumoServicio.loggger(false);

		if (SocketTimeoutException.class == resourceException.getMostSpecificCause().getClass()) {
			LOGGER.error(urlServicio  + Constantes.TIME_OUT,
					resourceException.getMostSpecificCause());
			return getErrorResponse(500, Constantes.LOG_TIME_OUT, resourceException.getCause().getMessage());

		} else {
			LOGGER.error(urlServicio + Constantes.LOG_SERVICIO_NO_DISPONIBLE,
					resourceException.getMostSpecificCause());
			return getErrorResponse(500, Constantes.LOG_SERVICIO_NO_DISPONIBLE,
					resourceException.getCause().getMessage());
		}

	}

	/**
	 * Valida que tipo de Exception que se presenta mediante ExecutionException
	 *
	 * @param ResourceAccessException Exception generada por no respuesta del Servicio
	 * @param bitacoraConsumoServicio objeto para el almacenamiento de Logs
	 *
	 */
	public void validaException(ExecutionException executionException, String nombreServicio,
			String urlServicio, String request, String response, BitacoraConsumoServicio bitacoraConsumoServicio) {

		String mensajeError = (SocketTimeoutException.class == executionException.getCause().getClass())
				?  urlServicio + " |Request|" +  request + " |Response|" +  response + Constantes.TIME_OUT
				:  urlServicio + " |Request|" +  request + " |Response|" +  response +  Constantes.LOG_SERVICIO_NO_DISPONIBLE;

		LOGGER.error(mensajeError, executionException.getCause());

		bitacoraConsumoServicio.setResponseDate(new Date());
		bitacoraConsumoServicio.loggger(false);
		imprimeBitacora(urlServicio,request,response);
	}

	public void imprimeBitacora(String urlServicio,String request, String response) {

		LOGGER.info("Bitacora::: " + urlServicio + " |Request| " + request + " |Response|" + response);
	}
	
	public ResponseEntity<Object>  sendPostObjectToObject(String url, Object o,String urlBitacora,
			String operacion,HttpHeaders headers,Map<String, Object> mapBody,
			boolean envioAbitacora,String tipoOperacion){

		RestTemplate restTemplate =new RestTemplate();
		Object objetoResponse =  restTemplate.postForObject(url, o, Object.class);
		System.out.println("response " + objetoResponse.toString());

		if(objetoResponse.toString().indexOf("Stream closed")>0)
		{
			 ErrorDTO error=new ErrorDTO("Error en variables requeridas["+o.toString()+"].Favor de validar.",objetoResponse.toString(), 400);
			 return new ResponseEntity<Object> ((Object) error,HttpStatus.BAD_REQUEST);
		};

		ResponseEntity<Object> response =new ResponseEntity<Object> (objetoResponse,HttpStatus.OK);

		if(envioAbitacora)
			registroBitacora(urlBitacora,operacion,headers,mapBody,tipoOperacion);

		return response;
	}
	private void registroBitacora(String urlBitacora, String operacion,HttpHeaders httpHeadersBitacora,Map<String,Object> body,String tipoOperacion) 
	{
    	LOGGER.info("registroBitacora");
		BitacoraConsumoServicio bitacoraConsumoServicio = new BitacoraConsumoServicio();
        bitacoraConsumoServicio.setService(tipoOperacion);
        bitacoraConsumoServicio.setIpUser(bitacoraConsumoServicio.getHostname());
        String mapJsonStr= mapToJson((Map<String,Object>)body);
        bitacoraConsumoServicio.setRequestValue(mapJsonStr);
        bitacoraConsumoServicio.setClientNumber(body.containsKey("cliente")?body.get("cliente").toString():"");
        bitacoraConsumoServicio.setRequestDate(new Date());
        bitacoraConsumoServicio.setHttpHeaders(httpHeadersBitacora);
		bitacoraConsumoServicio.setResponseDate(new Date());
		String headJsonStr= ((Map<String,String>)httpHeadersBitacora.toSingleValueMap()).toString(); 
		bitacoraConsumoServicio.loggger(true,urlBitacora);
    }
}
