package mx.com.beo.util;
  
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture; 

public class Multiples {
	
	
	private ListenableFuture<ResponseEntity<Object>> future;
	private String keyRest;
	private String url;
	private String request;
	
	public ListenableFuture<ResponseEntity<Object>> getFuture() {
		return future;
	}
	public void setFuture(ListenableFuture<ResponseEntity<Object>> future) {
		this.future = future;
	}
	public String getKeyRest() {
		return keyRest;
	}
	public void setKeyRest(String keyRest) {
		this.keyRest = keyRest;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
}
