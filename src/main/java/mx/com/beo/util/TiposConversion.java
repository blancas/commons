package mx.com.beo.util;

public enum TiposConversion {
	STRING,
	INTEGER,
	LONG,
	DOUBLE,
	FLOAT,
	BOOLEAN
}
