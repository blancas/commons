package mx.com.beo.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfiguracionGeneral {
	private static Map<String, Object> configuracionHistorial = System.getenv("CONFIGURACION_HISTORIAL") != null ? UtilidadesRest.jsonToMap(System.getenv("CONFIGURACION_HISTORIAL")) : null;
	private static Map<String, String> catalogoIdentificadores = new HashMap<>();
	private static Map<String, Object> configuracionJson = System.getenv("LOG_ESB_ENMASCARAMIENTO") != null ? UtilidadesRest.jsonToMap(System.getenv("LOG_ESB_ENMASCARAMIENTO")): null;
	private static List<Object> configuracionEnmascaramientoLogESB = configuracionJson != null && configuracionJson.containsKey("config") ? (List<Object>)configuracionJson.get("config") : null;

	private ConfiguracionGeneral(){
		catalogoIdentificadores.put("1", "Cuentas propias");
		catalogoIdentificadores.put("2", "Cuentas terceros multiva");
		catalogoIdentificadores.put("3", "Interbancarios");
		catalogoIdentificadores.put("4", "Creditos");
		catalogoIdentificadores.put("5", "Servicios");
		catalogoIdentificadores.put("6", "Pago tarjetas de credito");
		catalogoIdentificadores.put("7", "Departamentales");
		catalogoIdentificadores.put("8", "Ordenes de pago");
		catalogoIdentificadores.put("9", "Ordenes de pago Internacionales");
		catalogoIdentificadores.put("10", "Pago referenciado");
		catalogoIdentificadores.put("11", "Pago a proveedor tercero");
		catalogoIdentificadores.put("12", "Donaciones");
		catalogoIdentificadores.put("13", "Pago a proveedor interbancario");
	}
	
	public static List<Object> getConfiguracionEnmascaramientoLogESB() {
		return configuracionEnmascaramientoLogESB;
	}

	public static Map<String, Object> getConfiguracionHistorial() {
		return configuracionHistorial;
	}
	
	public static Map<String, String> getCatalogoIdentificadores(){
		return catalogoIdentificadores;
	}
	
}
