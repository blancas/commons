package mx.com.beo.util;

import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class BitacoraConsumoServicio {
	Date requestDate;
	Date responseDate;
	String timeFormat;
	SimpleDateFormat ft;
	String service;
	String clientNumber;
	String userlog;
	String ipUser;
	String requestValue;
	String responseValue;
	HttpHeaders httpHeaders;
	Marker bitacoraMarker;
	Map<String, Object> requestValueMap;
	Map<String, Object> responseValueMap;
	
	public BitacoraConsumoServicio(){
		ft = new SimpleDateFormat ("dd-MM-yyyy'T'HH:mm:ss");
//		ft = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		bitacoraMarker = MarkerFactory.getMarker("bitacora");
	}
	
	void setDateFormat(String format){
		ft = new SimpleDateFormat (format);
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BitacoraConsumoServicio.class);
	
	void loggger(boolean setBitacora){
		loggger(setBitacora,null);
	}
	public void loggger(boolean setBitacora,String urlHistorial){
		String safeRequest = enmascarar(getRequestValue(),getService(),"xxxx","request");
		String safeResponse = enmascarar(getResponseValue(),getService(),"xxxx","response");
		LOGGER.error(
				bitacoraMarker,
				"{}|{}|{}|{}|{}|{}|{}",
				getRequestDate(),
				getService(),
				getClientNumber(),
				//getUserlog(),
				getIpUser(),
				safeRequest,
				getResponseDate(),
				safeResponse
				);
		
		if(setBitacora){
			LOGGER.debug("registroBitacora {}:{}, urlBitacora:{}",getService(),setBitacora,urlHistorial);
			registroBitacora(urlHistorial, getService());
		}
	}
	
	/**
	 * @author Luis Adrian Blancas
	 * @param setBitacora
	 * @param urlHistorial
	 */
	public void logggerMultiPartRequest(boolean setBitacora,String urlHistorial){
		String safeRequest = enmascarar(getRequestValue(),getService(),"xxxx","request");
		String safeResponse = enmascarar(getResponseValue(),getService(),"xxxx","response");
		LOGGER.error(
				bitacoraMarker,
				"{}|{}|{}|{}|{}|{}|{}",
				getRequestDate(),
				getService(),
				getClientNumber(),
				//getUserlog(),
				getIpUser(),
				safeRequest,
				getResponseDate(),
				safeResponse
				);
		
		if(setBitacora){
			LOGGER.debug("registroBitacora {}:{}, urlBitacora:{}",getService(),setBitacora,urlHistorial);
			registroBitacoraMultiPartRequest(urlHistorial, getService());
		}
	}
	
	void loggger(boolean setBitacora, String urlHistorial,String tipoArchivo){
		String safeRequest = enmascarar(getRequestValue(),getService(),"xxxx","request");
		String safeResponse = enmascarar(getResponseValue(),getService(),"xxxx","response");
		LOGGER.error(
				bitacoraMarker,
				"{}|{}|{}|{}|{}|{}|{}",
				getRequestDate(),
				getService(),
				getClientNumber(),
				getUserlog(),
				getIpUser(),
				safeRequest,
				getResponseDate(),
				safeResponse
				);
		
		if(setBitacora){
			LOGGER.debug("registroBitacora {}:{}, urlBitacora:{}, dato:{}",getService(),setBitacora,urlHistorial,tipoArchivo);
		 
			registroBitacoraMulti(urlHistorial.toString(), getService());
		}
	}

	/**
	 * @author Luis Blancas 
	 * @param  urlHistorial
	 * @param  tipoOperacion
	 */
	public void registroBitacoraMultiPartRequest(String urlHistorial, String tipoOperacion){
		LOGGER.debug("registroBitacoraMultiPartRequest::::tipoOperacion = {}", tipoOperacion);

		try{
			RestTemplate restTemplate = new RestTemplate();
			Map<String, Object> documento = new HashMap<>();

			
			
			documento.put("fecha", new SimpleDateFormat ("dd-MM-yyyy'T'HH:mm:ss").format(this.requestDate));
			documento.put("tipoOperacion",tipoOperacion);
			documento.put("estatus","exito");
			validaFolioMultivanet(documento);
			Map<String, String> headers = getHttpHeaders().toSingleValueMap(); 
			boolean ivRemote = (String) headers.get("iv-remote-address")!=null;
			if(ivRemote){
				documento.put("IP", headers.get("iv-remote-address"));
			}else{
				documento.put("IP", getIpUser());
			}
			registroBitacoraCasosEspeciales(documento,tipoOperacion);
			
			Map<String, Object> cuerpoPeticion = new HashMap<>();
			cuerpoPeticion.put("operacion", "A");
			cuerpoPeticion.put("documento", documento);
			System.out.println("urlHistorialssss.........."+ urlHistorial);
			HttpHeaders jsonHeader = new HttpHeaders();
			Map<String, Object> mapaHeaders = (Map) this.httpHeaders.toSingleValueMap();
			jsonHeader.setContentType(MediaType.APPLICATION_JSON);
			String cookie="cookie";
			String iv_user="iv-user";
			String cliente="cliente";
			String numero_cliente="numero-cliente";
			String tipocanal="tipocanal";
			if(this.httpHeaders.containsKey(iv_user)) 
				jsonHeader.add(iv_user, (String) mapaHeaders.get(iv_user) ); 
			if(this.httpHeaders.containsKey(cookie)) 
				jsonHeader.add(cookie, (String) mapaHeaders.get(cookie) );
			if(this.httpHeaders.containsKey(cliente)) 
				jsonHeader.add(cliente, (String) mapaHeaders.get(cliente) ); 
			if(this.httpHeaders.containsKey(numero_cliente)) 
				jsonHeader.add(numero_cliente, (String) mapaHeaders.get(numero_cliente) );
			if(this.httpHeaders.containsKey(tipocanal)) 
				jsonHeader.add(tipocanal, (String) mapaHeaders.get(tipocanal) );
			
			restTemplate.exchange(urlHistorial, HttpMethod.POST,new HttpEntity<>(cuerpoPeticion, jsonHeader), Object.class);

		}catch (Exception e) {
			LOGGER.error("urlHistorico: {}", urlHistorial);
			LOGGER.error("Servicio bitacora: {}", e.getMessage());
			e.printStackTrace();
		}
	}

	public void registroBitacoraMulti(String urlHistorial, String tipoOperacion){
		LOGGER.debug("tipoOperacion = {}", tipoOperacion);
		try{ 
			RestTemplate restTemplate = new RestTemplate();
			 
			Map<String, Object> documento = new HashMap<>();
			  
			documento.put("fecha", new SimpleDateFormat ("dd-MM-yyyy'T'hh:mm:ss").format(this.requestDate));
			documento.put("tipoOperacion",tipoOperacion); 
			
			Map<String, String> headers = getHttpHeaders().toSingleValueMap(); 
			boolean ivRemote = (String) headers.get("iv-remote-address")!=null;
			if(ivRemote){
				documento.put("IP", headers.get("iv-remote-address"));
			}else{
				documento.put("IP", getIpUser());
			}
			
			registroBitacoraCasosEspeciales(documento,tipoOperacion); 
			
			Map<String, Object> cuerpoPeticion = new HashMap<>();
			
			cuerpoPeticion.put("operacion", "A");
			cuerpoPeticion.put("documento", documento); 
			
			this.httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);  
			  
			HttpEntity<Map<String, Object>> requestEntity = new 
					HttpEntity<Map<String, Object>>(cuerpoPeticion, this.httpHeaders); 

			 restTemplate.exchange(urlHistorial, HttpMethod.POST, requestEntity, Object.class);  
			 
		}catch (Exception e) {
			LOGGER.error("urlHistorico: {}", urlHistorial);
			LOGGER.error("Servicio bitacora: {}", e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	private void registroBitacoraCasosEspecialesMapeo(String servicioConversion, ArrayList<Object> mapeoDetalle, Map<String, Object> documento, String tipoOperacion){
		
		
			if(servicioConversion.equals(tipoOperacion) && mapeoDetalle!=null){
				LOGGER.debug("Caso especial: {}", servicioConversion);
				for (Object detalle : mapeoDetalle) {
					Map<String, Object> configuracionMapa = (Map<String, Object>)detalle;
					String funcion = configuracionMapa.containsKey("funcion")?(String)configuracionMapa.get("funcion"):"";
					String llave = configuracionMapa.containsKey("llave")?(String)configuracionMapa.get("llave"):"";
					Object valor = configuracionMapa.containsKey("valor")?configuracionMapa.get("valor"):null;

					documento.put(llave, registroBitacoraCasosEspecialesAplicaFuncion(llave, valor, funcion));
				}
			}
		
	}
	
	private Object registroBitacoraCasosEspecialesAplicaFuncion(String llave, Object valor, String funcion){
		Object valorFinal = null;
		if(!llave.equals("") && valor != null && !valor.equals("")){
			LOGGER.debug("Aplicar funcion {}", funcion);
			switch (funcion) {
				case "catalogoIdentificadores":
					valorFinal = catalogoIdentificadores(valor);
				break;
				case "formatoFecha":
					valorFinal = formatoFecha(valor);
				break;
				default:
					valorFinal = valorDeContexto(valor);
				break;
			}
		}
		return valorFinal;
	}
	
	private void registroBitacoraCasosEspeciales(Map<String, Object> documento, String tipoOperacion){
		 
		Map<String, Object> configuracionBitacoraHistorial = ConfiguracionGeneral.getConfiguracionHistorial();
		ArrayList<String> listaExcepcion  = configuracionBitacoraHistorial != null && configuracionBitacoraHistorial.containsKey("excepcion")  ? (ArrayList<String>) configuracionBitacoraHistorial.get("excepcion") : null;
		ArrayList<Object> listaConversion = configuracionBitacoraHistorial != null && configuracionBitacoraHistorial.containsKey("conversion") ? (ArrayList<Object>) configuracionBitacoraHistorial.get("conversion"): null;
		
		if(listaExcepcion!=null && listaExcepcion.contains(tipoOperacion)){
			LOGGER.debug("tipoOperacion [{}] es un caso excepcion", tipoOperacion);
			return;
		}
		
		if(listaConversion == null){
		    LOGGER.debug("tipoOperacion [{}] no usa conversion", tipoOperacion);
		    return;
		}

		if(LOGGER.isDebugEnabled()){
			String listaConversiones = listaConversion.toString();
			LOGGER.debug("listaConversion: {}"+listaConversiones);
		}
		
		for(Object item : listaConversion){
			Map<String, Object> mapeoGeneral = (Map<String, Object>)item;
			String servicioConversion = mapeoGeneral.containsKey(Constantes.MAPA_SERVICIO)?(String)mapeoGeneral.get(Constantes.MAPA_SERVICIO):"";
			ArrayList<Object> mapeoDetalle = mapeoGeneral.containsKey("mapeo")?(ArrayList<Object>)mapeoGeneral.get("mapeo"):null;
			
			registroBitacoraCasosEspecialesMapeo(servicioConversion, mapeoDetalle, documento, tipoOperacion);
		}
	}
	
	public void registroBitacora(String urlHistorial, String tipoOperacion){
		LOGGER.debug("tipoOperacion = {}", tipoOperacion);

		try{
			RestTemplate restTemplate = new RestTemplate();
			Map<String, Object> documento = new HashMap<>();

			validaFolioMultivanet(documento);
			
			documento.put("fecha", new SimpleDateFormat ("dd-MM-yyyy'T'HH:mm:ss").format(this.requestDate));
			documento.put("tipoOperacion",tipoOperacion);
			documento.put("estatus","exito");
			 
			Map<String, String> headers = getHttpHeaders().toSingleValueMap(); 
			boolean ivRemote = (String) headers.get("iv-remote-address")!=null;
			if(ivRemote){
				documento.put("IP", headers.get("iv-remote-address"));
			}else{
				documento.put("IP", getIpUser());
			}
			registroBitacoraCasosEspeciales(documento,tipoOperacion);
			
			Map<String, Object> cuerpoPeticion = new HashMap<>();
			cuerpoPeticion.put("operacion", "A");
			cuerpoPeticion.put("documento", documento);
			System.out.println("urlHistorialssss.........."+ urlHistorial);
			LOGGER.info("Bitacora cuerpoPeticion: {}", cuerpoPeticion);
			restTemplate.exchange(urlHistorial, HttpMethod.POST, new HttpEntity<>(cuerpoPeticion, this.httpHeaders), Object.class);

		}catch (Exception e) {
			LOGGER.error("urlHistorico: {}", urlHistorial);
			LOGGER.error("Servicio bitacora: {}", e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void validaFolioMultivanet(Map<String, Object> documento) {

		switch(getService()){
		
			case Constantes.SERVICIO_EJECUTA_TRANSACCION:	
				int existeResult = getResponseValue().toString().indexOf("result");
				
				if(existeResult > -1){
					JSONObject jsonObj = new JSONObject(getResponseValue().toString());	
					int existe = jsonObj.get("result").toString().indexOf("folioMultivanet");
					
					if(existe > -1){
						JSONObject jsonObj2 = new JSONObject(jsonObj.get("result").toString());
						String folioMultivaNet = jsonObj2.get("folioMultivanet") != null && !jsonObj2.get("folioMultivanet").equals("") ? jsonObj2.get("folioMultivanet").toString() : "";
						documento.put("autorizacion", folioMultivaNet);
					}
				}
				break;
				
			default: 
				LOGGER.info("El servicio {} no cuenta con folioMultivanet", getService());
		}
	}

	private Object catalogoIdentificadores(Object argumentos){
		Map<String, String> catalago = ConfiguracionGeneral.getCatalogoIdentificadores();
		Object valor = valorDeContexto(argumentos);
		Object resultado;
		String id="";
		LOGGER.debug("catalogoIdentificadores argumento: {}", argumentos);
		LOGGER.debug("catalogoIdentificadores valor: {}", valor);
		if(valor != null && valor.getClass() == String.class)
			id = (String)valor;
		if(valor != null && valor.getClass() == Integer.class)
			id = ((Integer)valor).toString();
		
		resultado = catalago.containsKey(id) ? catalago.get(id):null;
		LOGGER.debug("catalogoIdentificadores resultado: {}", resultado);
		return resultado;
	}
	
	private Object formatoFecha(Object argumentos){
		LOGGER.debug("formatoFecha paso 1");
		String fechaHoraRaw = "";
		ArrayList<Object> parametros= null;
		SimpleDateFormat formatoEntrada = null;
		SimpleDateFormat formatoSalida = null;
		Date fechaEntrada = null;
		Object fechaSalida = null;
		LOGGER.debug("formatoFecha paso 2");
		if(argumentos != null && argumentos.getClass()==ArrayList.class && ((ArrayList)argumentos).size()==4){
			LOGGER.debug("formatoFecha paso 3");
			parametros = (ArrayList<Object>)argumentos;
			LOGGER.debug("formatoFecha paso 4");
			fechaHoraRaw = (String)valorDeContexto(parametros.get(0)) + (String)valorDeContexto(parametros.get(1));
			LOGGER.debug("formatoFecha paso 5");
			formatoEntrada = new SimpleDateFormat((String)parametros.get(2));
			LOGGER.debug("formatoFecha paso 6");
			formatoSalida = new SimpleDateFormat((String)parametros.get(3));
			LOGGER.debug("formatoFecha paso 7");
			try {
	        	fechaEntrada = formatoEntrada.parse(fechaHoraRaw);
	        	fechaSalida  = formatoSalida.format(fechaEntrada);
	        } catch (ParseException e) {
	            LOGGER.error(e.getMessage());
	        }
		}
		return fechaSalida;
	}
	
	private Object valorDeContexto(Object argumento){
		Object resultado = null;
		Object data = null;
		
		if(argumento.getClass() != String.class){
			return null;
		}
		
		String[] path = ((String)argumento).split("\\.");
		if(path[0].equals("response")){
			LOGGER.debug("valorDeContexto response");
			if(responseValueMap == null){
				responseValueMap = UtilidadesRest.jsonToMap(responseValue);
			}
			data = responseValueMap;
		}
			
		if(path[0].equals("request")){
			LOGGER.debug("valorDeContexto request");
			if(requestValueMap == null){
				requestValueMap = UtilidadesRest.jsonToMap(requestValue);
			}
			data = requestValueMap;
		}
		
		if(data == null)
			return null;
			
		for(int i=1; i < path.length;i++){
			if(resultado == null)
				resultado = ((Map<String, Object>)data).get(path[i]);
			else
				resultado = ((Map<String, Object>)resultado).get(path[i]);
		}

		return resultado;
	}
	
	public String getMethodFromURL(String url){
        try{
        	String[] urlWhithoutParapeters = url.split("\\?");
        	String[] urlElemnts = urlWhithoutParapeters[0].split("/");
        	return urlElemnts[urlElemnts.length-1];
        }catch (Exception e) {
        	return "";
		}
	}
	
	public String getHostname(){
        String hostName ="";
        try{
        InetAddress iAddress = InetAddress.getLocalHost();
        hostName = iAddress.getHostName();
        }catch (Exception e) {
        	hostName = System.getenv("HOSTNAME");
		}
        return hostName;
	}
	
	public static String enmascararDatosJson(String cadenaOriginal, String key, String nuevoValor){
		Pattern ptn = Pattern.compile("(?iu)\""+key+"\"(:\\\"(?s)(.*?)\\\")|\""+key+"\":(?s)\\d*");
		Matcher mtch = ptn.matcher(cadenaOriginal);
		return mtch.replaceAll("\""+key+"\":\""+nuevoValor+"\"");
	}
		  
	public static String enmascaraCadena(String cadenaOriginal,String[] keyList, String nuevoValor){
		if(keyList.length>1)
			return enmascaraCadena(enmascararDatosJson(cadenaOriginal,keyList[0],nuevoValor),Arrays.copyOfRange(keyList, 1, keyList.length),nuevoValor);
		else{
			return enmascararDatosJson(cadenaOriginal,keyList[0],nuevoValor);
		}
	}
	
	public HttpHeaders getHttpHeaders() {
		return httpHeaders;
	}

	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	/**
	 * Oculta los valores sensibles de las variables indicadas en el JSON de configuración.
	 * JSON de configuración -> {"config":[{"servicio":"cambioContrasena","request":["oldPassword","newPassword","confirmNewPassword"],"response":["codigo"]}]}
	 * @param cadena Valor original
	 * @param servicio nombre del servicio correspondiente a cadena.
	 * @param nuevoValor cadena que sera utilizado para representar los caracteres que deben ser enmascarados.
	 * @param tipoCadena se refiere a si es una cadena tipo request o tipo response, un servicio puede requerir enmascaramiento en tan solo en un tipo de mensaje.
	 * @return nueva cadena con las valores enmascarados de variables con datos sensibles. 
	 */
	public String enmascarar(String cadena, String servicio, String nuevoValor, String tipoCadena){
		
		
		List<Object> configuracion = ConfiguracionGeneral.getConfiguracionEnmascaramientoLogESB();
		if(configuracion != null)
			for(Object item : configuracion){
				Map<String,Object> servicioConfiguracion = (Map<String,Object> )item;
				if(servicioConfiguracion != null && servicioConfiguracion.containsKey(Constantes.MAPA_SERVICIO) && servicio.equals(servicioConfiguracion.get(Constantes.MAPA_SERVICIO)) && servicioConfiguracion.containsKey(tipoCadena)){
					List<String> lista  = (List<String>)servicioConfiguracion.get(tipoCadena);
					cadena = enmascaraCadena(cadena, lista.toArray( new String[ lista.size() ] ), nuevoValor);
				}
			}
		return cadena;
	}
	
	public String getRequestDate() {
		return this.requestDate != null ? ft.format(this.requestDate):"";
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getResponseDate() {
		return this.responseDate!= null ? ft.format(this.responseDate):"";
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public String getService() {
		return this.service != null ? this.service:"";
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getClientNumber() {
		return this.clientNumber != null ? this.clientNumber:"";
	}

	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}

	public String getUserlog() {
		return this.userlog != null ? this.userlog : "";
	}

	public void setUserlog(String userlog) {
		this.userlog = userlog;
	}

	public String getIpUser() {
		return this.ipUser != null ? this.ipUser : "";
	}

	public void setIpUser(String ipUser) {
		this.ipUser = ipUser;
	}

	public String getRequestValue() {
		return this.requestValue != null ? this.requestValue : "";
	}

	public void setRequestValue(String requestValue) {
		this.requestValue = requestValue;
	}

	public String getResponseValue() {
		return this.responseValue != null ? this.responseValue : "";
	}

	public void setResponseValue(String responseValue) {
		this.responseValue = responseValue;
	}
	
}
