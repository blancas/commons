package mx.com.beo.util;

public class Constantes {
	
private Constantes() {
		
	}
	//atributo JSON
	public static final String  RESPONSE_STATUS = "responseStatus";
	public static final String  RESPONSE_ERROR = "responseError";
	public static final String  MAPA_SERVICIO = "servicio";
	
	//Mensajes de error
	public static final String  ERROR_SOLICITUD = "Error al procesar solicitud";
	
	public static final String  PROTOCOLO = "PROTOCOLO";
	public static final String  HOSTNAME_BEO = "HOSTNAME_BEO";
	public static final String  PUERTO = "PUERTO";
	public static final String  BASEPATH = "BASEPATH";
	public static final String  BITACORA_URL = "BITACORA_URL";
	
	public static final String TIME_OUT = " - TimeOut";
	public static final String LOG_SERVICIO_NO_DISPONIBLE = " - Servicio no disponible";
	public static final String LOG_TIME_OUT = "TimeOut en la Solicitud";
	
	public static final String SERVICIO_EJECUTA_TRANSACCION = "ejecutaTransaccion";
}