package mx.com.beo.exceptions;

/**
 * Created by raul on 8/11/17.
 */
public class HeaderNotFoundException extends Exception
{
    public HeaderNotFoundException() {
    }

    public HeaderNotFoundException(String message) {
        super(message);
    }
}
