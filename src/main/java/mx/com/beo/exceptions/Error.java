package mx.com.beo.exceptions;

public class Error {

		String response;
		int    code;
		String url;
		
		
		public Error(String response, int code, String url) {
			super();
			this.response = response;
			this.code = code;
			this.url = url;
		}
		public String getResponse() {
			return response;
		}
		public void setResponse(String response) {
			this.response = response;
		}
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		@Override
		public String toString() {
			return response + ", url=" + url ;
		}
		
}
