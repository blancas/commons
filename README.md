# Commons

Funciones comunes para ser utilizadas por los microservicios

## Uso local

Compilar el proyecto 
```
mvn clean install
```

## Subir artefacto al repositorio remoto

Para que pueda ser utilizado como una dependencia, es necesario instalar el proyecto en el repositorio remoto realizando las siguientes acciones:

1.- Configurar el settings.xml de maven

```xml
    <server>
      <id>nexus-repository</id>
      <username>middleware</username>
      <password>m1ddl3w4r3</password>
    </server>
```

2.- Agregar el siguiente plugin al **pom.xml**

```xml
    <distributionManagement>
		<repository>
			<id>nexus-repository</id>
			<url>http://200.39.24.141:8081/repository/ADMiddleware/</url>
		</repository>
	</distributionManagement>
```

3.- Empaquetar el proyecto

```
mvn clean deploy

```

## Uso como dependecia 

Para utilizar la libreria como dependencia es necesario agregar al **pom.xml** del proyecto las siguientes etiquetas:

```xml
      <dependency>
         <groupId>mx.com.beo</groupId>
         <artifactId>commons</artifactId>
         <version>${commons.version}</version>
      </dependency>
```

```xml
   <repositories>
      <repository>
        <id>nexus-repository</id>
        <url>http://200.39.24.141:8081/repository/GrupoMiddleware</url>
      </repository>
   </repositories>  
```

##### ENMASCARAMIENTO
En caso de ser necesario enmascarar alguna variable correspondiente al Request o Response en el archivo de bitacora Multiva sera necesario utilizar la siguiente variable de ambiente para indicar la configuración necesaria.
NOTA: en caso de no requerir enmascarar ninguna información no sera necesario configurar esta variable de ambiente
```
LOG_ESB_ENMASCARAMIENTO
Ejemplo:
LOG_ESB_ENMASCARAMIENTO={"config":[{"servicio":"cambioContrasena","request":["oldPassword","newPassword","confirmNewPassword"],"response":[]}]}
* Explicacion:
{
  "config":[
    {
      "servicio":
      "request":[],
      "response":[]
    }
  ]
}
config: arreglo de objetos, cada objeto representa la configuracion de un servicio.
servicio: nombre del endpoind del servicio que se esta configurando.
request: es un arreglo con el nombre de todos los campos que deben ser enmascarados que se encuentran en el Request que invoca al servicio.
response: es un arreglo con el nombre de todos los campos que deben ser enmascarados que se encuentran en el Response que respondio el servicio.
```

##### CARACTERISTICAS COMPLEMENTARIAS
Para la configuracion del log en ficheros se debe configurar estas variables de ambiente.

    Directorio para guardar el log
    DIR_LOG=/tmp/transaccion

    LOG_LEVEL_GENERAL
        Default:ERROR
        Posibles valores: [TRACE,DEBUG,INFO,WARN,ERROR]
    LOG_LEVEL_BITACORA
        Default:ERROR
        Posibles valores: [TRACE,DEBUG,INFO,WARN,ERROR]}


##### NUEVA FUNCIONALIDAD.

Se agrego un nuevo metodo que nos permite mandar un archivo

Set<MediaType> mediaTypeValidos = new HashSet<>();
			mediaTypeValidos.add(MediaType.APPLICATION_JSON);
			mediaTypeValidos.add(MediaType.APPLICATION_JSON_UTF8);
			mediaTypeValidos.add(MediaType.APPLICATION_OCTET_STREAM);

MultiValueMap<String, Object> parts = 
		          new LinkedMultiValueMap<String, Object>();

Se manda falso cuando nos va a mandar un JSON y true cuando nos va a regresar un APPLICATION_OCTET_STREAM

enviarPeticion(url,HttpMethod.POST,mediaTypeValidos, parts, false, Urls.URL_BITACORA.getPath());






